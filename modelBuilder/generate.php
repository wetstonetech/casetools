<?php 

if(isset($_POST['submit'])) {
	 
	function getDAOHeader($className) {
		$header = '<?php

import (\''. strtolower($className).'.'. ucfirst(strtolower($className)).'\');
		
class '. ucfirst(strtolower($className)).'DAO extends DAO {
				';
		return $header;
	}

	 function getDAOConstructor($className, $variables) {
		$constructor = '
		
		/** Cached array of users by userName */
		var $'. strtolower($className).'Cache;

		/**
		* Constructor.
		*/
		function '. ucfirst(strtolower($className)).'DAO() {
			parent::DAO();
			$this->'. strtolower($className).'Cache = array();
			$result = &$this->retrieve(
				\'SELECT * FROM information_schema.tables WHERE TABLE_NAME =  "'. strtolower($className) .'" AND TABLE_SCHEMA = ? \', Config::getVar("database","name")
			);
			if($result->RecordCount() == 0){
				$this->createTable();
			}
		}
	
		function createTable(){
			$file = fopen(ApplicationCore::getBaseDir().\'/classes/'.strtolower($className).'/db/'. ucfirst(strtolower($className)).'.sql\',\'r\');
			$sql = stream_get_contents($file);
			$result = &$this->update($sql);
			fclose($file);
		}
	
		function &get'. ucfirst(strtolower($className)).'ById($'. strtolower($className).'Id){
			// First check the in-memory response cache
			if (isset($this->'. strtolower($className).'Cache[$'. strtolower($className).'Id])) {
				return $this->'. strtolower($className).'Cache[$'. strtolower($className).'Id];
			}
			
			$result = &$this->retrieve(
				\'SELECT * FROM '. strtolower($className).' WHERE '. strtolower($className).'_id = ? \', array($'. strtolower($className).'Id)
			);

			$returner = null;
			if ($result->RecordCount() != 0) {
				$returner = &$this->_return'. ucfirst(strtolower($className)).'FromRow($result->GetRowAssoc(false));
			}
			$result->Close();
			unset($result);

			// Cache this '. ucfirst(strtolower($className)).'.
			$this->'. strtolower($className).'Cache[$'. strtolower($className).'Id] =& $returner;

			return $returner;
	
		}
	
		function _return'. $className.'FromRow(&$row){
			$'. strtolower($className).' = &new '. ucfirst(strtolower($className)).'();	
			
			$'. strtolower($className).'->set'. ucfirst(strtolower($className)).'Id($row[\''. strtolower($className).'_id\']);';
			
			 foreach($variables as $varable){
				$parts = explode(':',$varable);
				$varable = $parts[0];
				$constructor .= '
			$'. strtolower($className).'->set'.ucfirst(strtolower($varable)) .'($row[\''. strtolower($varable).'\']);';
			}
			

		$constructor .= '
		
			HookRegistry::call(\''. ucfirst(strtolower($className)).'::_return'. ucfirst(strtolower($className)).'FromRow\', array(&$'. strtolower($className).', &$row));
			return $'. strtolower($className).';
		}
	
		function &get'. $className.'s(){
			$result = &$this->retrieveRange(
				\'SELECT * FROM '. strtolower($className).'\'
			);
		
			$returner =& new DAOResultFactory($result, $this, \'_return'. ucfirst(strtolower($className)).'FromRow\');
			return $returner;
		}';
		return $constructor;
		
	 }
	 
	 function getDAONew($className, $variables) {
		
		$new = '
		
		/**
		* Insert a new '. $className.'.
		* @param $'. strtolower($className).' '. ucfirst(strtolower($className)).'
		*/	
	 	function insert'. ucfirst(strtolower($className)).'(&$'. strtolower($className).') {
			$this->update(
				\'INSERT INTO '. strtolower($className).'
					(' ;
					
					foreach ($variables as $varable) {
						$parts = explode(':',$varable);
						$varable = $parts[0];
						$new .= ' 
						'. strtolower($varable).',';
					}
					$new = substr($new, 0, -1);		
					$new .= ')
					VALUES
					(' ;
					
					foreach ($variables as $varable) {
						$parts = explode(':',$varable);
						$varable = $parts[0];
						$new .= '?,';
					}
					$new = substr($new, 0, -1);	
					$new .= ')\',
				array(
					 ';
					
					foreach ($variables as $varable) {
						$parts = explode(':',$varable);
						$varable = $parts[0];
						$new .= '
					 $'. strtolower($className).'->get'.ucfirst(strtolower($varable)).'(),';
					} 
				$new = substr($new, 0, -1);		
				$new .= ')
			);

			$'. strtolower($className).'Id = $this->getInsert'. ucfirst(strtolower($className)).'Id();
			$'. strtolower($className).'->set'. ucfirst(strtolower($className)).'Id($'. strtolower($className).'Id);

			// Cache this response.
			$this->'. strtolower($className).'Cache[$'. strtolower($className).'->get'. ucfirst(strtolower($className)).'Id()] =& $returner;

			return $'. strtolower($className).'->get'. ucfirst(strtolower($className)).'Id();
		}';
		
		return $new;
	}
	
	 function getDAOUpdate($className, $variables) {
		
		$new = '
		
	 	function update'. ucfirst(strtolower($className)).'(&$'. strtolower($className).') {
			return $this->update(
				\'UPDATE '. strtolower($className).' SET ';
					
					foreach ($variables as $varable) {
						$parts = explode(':',$varable);
						$varable = $parts[0];
						$new .= '
						'. strtolower($varable) .' = ?,';
					}
					$new = substr($new, 0, -1);
					$new .= ' 
					WHERE '.strtolower($className).'_id = ?';

					
					$new .= '\',
				array(';
					
					foreach ($variables as $varable) {
						$parts = explode(':',$varable);
						$varable = $parts[0];
						$new .= '
					 $'. strtolower($className).'->get'.ucfirst(strtolower($varable)).'(),';
					} 
					$new .= '
					 $'. strtolower($className).'->get'. ucfirst(strtolower($className)).'Id())';
					
					$new .= '
				);
		}';
		return $new;
	 }
		
	 function getDAOFooter($className) {
		$footer = '
		
		/**
	 	* Get the ID of the last inserted '.strtolower($className).'
		 * @return int
		 */
		function getInsert'. ucfirst(strtolower($className)).'Id() {
			return $this->getInsertId(\''. strtolower($className).'\', \''. strtolower($className).'_id\');
		}
}

?>';
		return $footer;
	 }
	 
	 
	 
	function getHeader($className) {
		$header = '<?php

class '. ucfirst(strtolower($className)).' extends DataObject {
				';
		return $header;
	}
	 

	function getConstructor($className, $variables) {
	
		$con = '
	
	function set'. ucfirst(strtolower($className)).'Id($'. strtolower($className).'Id){
		$this->setData(\''. strtolower($className).'Id\',$'. strtolower($className).'Id);
	}
	function get'. ucfirst(strtolower($className)).'Id(){
		return $this->getData(\''. strtolower($className).'Id\');
	}'. "\r\n";
	
	foreach ($variables as $varable) {
		$parts = explode(':',$varable);
		$varable = $parts[0];
		$con .= '		
	function set'. ucfirst(strtolower($varable)).'($'.strtolower($varable).'){
		$this->setData(\''. strtolower($varable).'\',$'. strtolower($varable).');
	}				
	function get'. ucfirst(strtolower($varable)).'(){
		return $this->getData(\''. strtolower($varable).'\');
		
	}'. "\r\n";
	}

	return $con;
	
	}
	
	 function getFooter($className) {
		$footer = '
}

?>';
		return $footer;
	 }




	function getFormHeader($className) {
		$fheader = '<?php

import(\'form.Form\');
class '. ucfirst(strtolower($className)).'Form extends Form {
				';
		return $fheader;
	}



	function getFormConstructor($className, $variables) {
	
		$fcon = '
	/** The ID of the '. strtolower($className).' being edited */
	var $'. strtolower($className).'Id;	

	
	
	/** The '. strtolower($className).' object */
	var $'. strtolower($className).';
	
	
	function '. ucfirst(strtolower($className)).'Form($'. strtolower($className).'Id = null){
		parent::Form(\''. strtolower($className).'/'. strtolower($className).'Form.tpl\');
		
		$this->'. strtolower($className).'Id = isset($'. strtolower($className).'Id) ? $'. strtolower($className).'Id : null;	
		
		if ($'. strtolower($className).'Id) {
			$'. strtolower($className).'Dao =& DAORegistry::getDAO(\''. ucfirst(strtolower($className)).'DAO\');
			$this->'. strtolower($className).' =& $'. strtolower($className).'Dao->get'. ucfirst(strtolower($className)).'ById($this->'. strtolower($className).'Id);		
		}
		
	}
	
	function display() {
		
		
		parent::display();
	}
	
	function initData() {
		if (isset($this->'. strtolower($className).')) {			
			$this->_data = array(
							\''. strtolower($className).'Id\' => $this->'. strtolower($className).'->get'. ucfirst(strtolower($className)).'Id()';
						foreach ($variables as $varable) {
							$parts = explode(':',$varable);
							$varable = $parts[0];
							$fcon .= ',
							\''.strtolower($varable).'\' => $this->'. strtolower($className).'->get'. ucfirst(strtolower($varable)).'()';
						}
			$fcon .= '
										);
		}	
	}
	
	function getParameterNames() {
		$parameterNames = array(';
		
						foreach ($variables as $varable) {
							$parts = explode(':',$varable);
							$varable = $parts[0];
							$fcon .= '
							\''.strtolower($varable).'\',';
						}
							$fcon .= '
							\''. strtolower($className).'Id\'
								);

		return $parameterNames;
	}
	
	function readInputData() {
		$this->readUserVars($this->getParameterNames());
	}
	
	function validate() {
		return parent::validate();
	}
	
	function execute() {
		$'. strtolower($className).'Dao = &DAORegistry::getDAO(\''. ucfirst(strtolower($className)).'DAO\');
		
		if (!isset($this->'. strtolower($className).')) {
			$this->'. strtolower($className).' = &new '. ucfirst(strtolower($className)).'();
		}';

		foreach ($variables as $varable) {
			$parts = explode(':',$varable);
			$varable = $parts[0];
			$fcon .= '
		$this->'. strtolower($className).'->set'. ucfirst(strtolower($varable)).'($this->getData(\''.strtolower($varable).'\'));';
		}
		
		$fcon .= ' 
		
		if ($this->'. strtolower($className).'->get'. ucfirst(strtolower($className)).'Id() != null) {
			$'. strtolower($className).'Dao->update'. ucfirst(strtolower($className)).'($this->'. strtolower($className).');
		} else {
			$'. strtolower($className).'Id = $'. strtolower($className).'Dao->insert'. ucfirst(strtolower($className)).'($this->'. strtolower($className).');		
		}
	}';
	
		return $fcon;

	}


	 function getFormFooter($className) {
		$ffooter = '
}

?>';
		return $ffooter;
	 }

	function getSQLCreate($className, $variables) {
		
		$sql = '
		CREATE TABLE '. strtolower($className).' (
			'. strtolower($className).'_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,';
		  foreach ($variables as $varable) {
			$parts = explode(':',$varable);
			$varable = $parts[0];
			$varableType = $parts[1];
			$sql .= '
			'. strtolower($varable).' '. strtoupper($varableType).' ,';
		  }
		$sql .= '
			PRIMARY KEY ('. strtolower($className).'_id)
		)
		ENGINE = MyISAM;';
		return $sql;

	}
	
	function getFormTpl($className, $variables) {
		$tpl .= '<form action="{url page="'.strtolower($className).'" op="update'.ucfirst(strtolower($className)).'"}">
		';
		$tpl .= '{if $'.strtolower($className).'Id}<input type="hidden" name="'.strtolower($className).'Id" value="{$'.strtolower($className).'Id}">{/if}
		';
		$tpl .= '<table>
		';

		foreach($variables as $variable) {
			$parts = explode(':',$variable);
			$variable = $parts[0];
			$tpl .= '<tr>
			';
			$tpl .= '<td>'.ucfirst(strtolower($className)).' '.$variable.'</td>
			';
			$tpl .= '<td><input type="text" size="25" name="'.strtolower($variable).'" value="{$'.strtolower($variable).'}"></td>
			';
			$tpl .= '</tr>
			';
		}
		$tpl .= '<tr>
		';
		$tpl .= '<td colspan="2"><input type="submit" size="25" value="Submit Form"></td>
		';
		$tpl .= '</tr>
		';
		$tpl .= '</table>
		';
		$tpl .= '</form>';
		return $tpl;
	}
	
	function getViewTpl($className, $variables) {
		$tpl .= '<table>
	';
		foreach($variables as $variable) {
			$parts = explode(':',$variable);
			$variable = $parts[0];
			$tpl .= '<tr>
			';
			$tpl .= '<td>'.ucfirst(strtolower($variable)).'</td>
			';
			$tpl .= '<td>{$'.strtolower($className).'->get'.ucfirst(strtolower($variable)).'()}</td>
			';
			$tpl .= '</tr>
			';
		}
		$tpl .= '
		</table>';
		return $tpl;
	}
	
	function getIndex($className, $variables) {
		$output .= '<?php
';
		
		$output .= "define('HANDLER_CLASS', '".ucfirst(strtolower($className))."Handler');
";

		$output .= "import('pages.".strtolower($className).".".ucfirst(strtolower($className))."Handler');
";
		$output .= '?>';
		
		return $output;
	}
	
	function getHandler($className, $variables) {
		$output .= '<?php
';
		$output .= 'class '.ucfirst(strtolower($className)).'Handler extends Handler {';
		$output .= '
	
	function edit'.ucfirst(strtolower($className)).'($args = array()) {
		$'.strtolower($className).'Id = $args[0];
	
		import("'.strtolower($className).'.form.'.ucfirst(strtolower($className)).'Form");
	
		$'.strtolower($className).'Form = new '.ucfirst(strtolower($className)).'Form($'.strtolower($className).'Id);
		$'.strtolower($className).'Form->initData();
		$'.strtolower($className).'Form->display();
	}
	
	function update'.ucfirst(strtolower($className)).'() {
		$'.strtolower($className).'Id = Request::getUserVar("'.strtolower($className).'Id");
	
		import("'.strtolower($className).'.form.'.ucfirst(strtolower($className)).'Form");
	
		$'.strtolower($className).'Form = new '.ucfirst(strtolower($className)).'Form($'.strtolower($className).'Id);
		$'.strtolower($className).'Form->initData();
		$'.strtolower($className).'Form->readInputData();
		if($'.strtolower($className).'Form->validate()) {
			$'.strtolower($className).'Form->execute();
			Handler::redirectMessage("msg.'.strtolower($className).'.update",array("'.strtolower($className).'","list'.ucfirst(strtolower($className)).'"));
		}else{
			$'.strtolower($className).'Form->display();
		}
	}
	
	function view'.ucfirst(strtolower($className)).'($args = array()) {
		$'.strtolower($className).'Dao =& DAORegistry::getDAO("'.ucfirst(strtolower($className)).'DAO");
		$'.strtolower($className).' = $'.strtolower($className).'Dao->get'.ucfirst(strtolower($className)).'ById($args[0]);
		
		$templateMgr =& TemplateManager::getManager();
		$templateMgr->assign("'.strtolower($className).'",$'.strtolower($className).');
		$templateMgr->display("'.strtolower($className).'/view'.ucfirst(strtolower($className)).'.tpl");
	}
	
	function list'.ucfirst(strtolower($className)).'s() {
		$'.strtolower($className).'Dao =& DAORegistry::getDAO("'.ucfirst(strtolower($className)).'DAO");
		$rangeInfo = Handler::getRangeInfo("'.strtolower($className).'s");
		$'.strtolower($className).'s = $'.strtolower($className).'Dao->get'.strtolower($className).'s($rangeInfo);
	
		$templateMgr =& TemplateManager::getManager();
		$templateMgr->assign("'.strtolower($className).'s",$'.strtolower($className).'s);
		$templateMgr->display("'.strtolower($className).'/list'.ucfirst(strtolower($className)).'s.tpl");
	}
}

		
?>';

		return $output;
	}
	
	

	/** Get Vars from Form **/
	$className= $_POST['className'];
	$classVariables = $_POST['classVariables'];	
	$variables = explode("\r\n",$classVariables);
 	 
	/** Build Paths **/
	$paths = array();
	$paths['root'] = './builds';
	$paths['object_name'] = './builds/'.strtolower($className);
	$paths['classes'] = './builds/'.strtolower($className).'/classes';
	$paths['class_object'] = './builds/'.strtolower($className).'/classes/'.strtolower($className);
	$paths['class_object_form'] = './builds/'.strtolower($className).'/classes/'.strtolower($className).'/form';
	$paths['db'] = './builds/'.strtolower($className).'/classes/'.strtolower($className).'/db';
	$paths['templates'] = './builds/'.strtolower($className).'/templates';
	$paths['template_object'] = './builds/'.strtolower($className).'/templates/'.strtolower($className);
	$paths['pages'] = './builds/'.strtolower($className).'/pages';
	$paths['pages_object'] = './builds/'.strtolower($className).'/pages/'.strtolower($className);
	
	/** Build Files **/
	$files = array();
	$files['dao'] = ucfirst(strtolower($className)).'DAO.inc.php';
	$files['object'] = ucfirst(strtolower($className)).'.inc.php';
	$files['form_object'] = ucfirst(strtolower($className)).'Form.inc.php';
	$files['sql'] = ucfirst(strtolower($className)).'.sql';
	$files['form_tpl'] = strtolower($className).'Form.tpl';
	$files['view_tpl'] = 'view'. strtolower($className).'.tpl';
	$files['handler'] = ucfirst(strtolower($className)).'Handler.inc.php';
	$files['index'] = 'index.php';
	
	/** Make Dirs **/
	@mkdir($paths['root']);
	@mkdir($paths['object_name']);
	@mkdir($paths['classes']);
	@mkdir($paths['class_object']);
	@mkdir($paths['class_object_form']);
	@mkdir($paths['db']);
	@mkdir($paths['templates']);
	@mkdir($paths['template_object']);
	@mkdir($paths['pages']);
	@mkdir($paths['pages_object']);
	
	/** Make Files **/
	$daoFile = fopen( $paths['class_object'].'/'. $files['dao'] ,'w+');	
	$dataObjectFile = fopen( $paths['class_object'].'/'. $files['object'] ,'w+');
	$formFile = fopen($paths['class_object_form'].'/'.$files['form_object'] ,'w+');
	$dbFile = fopen($paths['db'] .'/'. $files['sql'] ,'w+');
	$formTemplateFile = fopen($paths['template_object'].'/'. $files['form_tpl'] ,'w+');
	$viewTemplateFile = fopen($paths['template_object'] .'/'.$files['view_tpl'] ,'w+');
	$handlerFile = fopen($paths['pages_object'].'/'. $files['handler'] ,'w+');
	$indexFile = fopen($paths['pages_object'] .'/'.$files['index'] ,'w+');

	 fwrite ($daoFile,getDAOHeader($className));
	 fwrite ($daoFile,getDAOConstructor($className, $variables));
	 fwrite ($daoFile,getDAONew($className, $variables));
	 fwrite ($daoFile,getDAOUpdate($className, $variables));
	 fwrite ($daoFile,getDAOFooter($className));
	    
	 fwrite ($dataObjectFile,getHeader($className));
	 fwrite ($dataObjectFile,getConstructor($className, $variables));
	 fwrite ($dataObjectFile,getFooter($className));
	
	 fwrite ($formFile,getFormHeader($className));
	 fwrite ($formFile,getFormConstructor($className, $variables));
	 fwrite ($formFile,getFormFooter($className));
	 
	 fwrite ($dbFile,getSQLCreate($className, $variables));
	 
	 fwrite ($formTemplateFile,getFormTpl($className, $variables));
	 fwrite ($viewTemplateFile,getViewTpl($className, $variables));
	 
	 fwrite ($indexFile,getIndex($className, $variables));
	 fwrite ($handlerFile,getHandler($className, $variables));
	 
	 fclose ($daoFile);
	 fclose($dataObjectFile);
	 fclose($formFile);
	 fclose($dbFile);
	 fclose($formTemplateFile);
	 fclose($viewTemplateFile);
	 fclose($handlerFile);
	 fclose($indexFile);
	 
	 echo "<a href='index.php'>Build Another Model</a>";
}

?>