<?php 

if(isset($_POST['submit'])) {
	 
	function getDAOHeader($className) {
		$header = '<?php

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import nz.co.wetstone.db.DAO;
import nz.co.wetstone.db.DAOResultFactory;
import nz.co.wetstone.db.DBIterator;
		
public class '. ucfirst(strtolower($className)).'DAO extends DAO implements DBIterator {
				';
		return $header;
	}

	 function getDAOConstructor($className, $variables) {
		$constructor = '
		
			private static final String tableName = "'.strtolower($className).'";
			private String[] columns = new String[] { 
												"puzzle_id",';
	 		foreach($variables as $varable){
				$parts = explode(':',$varable);
				$varable = $parts[0];
				$constructor .= '
				$'. strtolower($className).'->set'.ucfirst(strtolower($varable)) .'($row[\''. strtolower($varable).'\']);';
			}
			$constructor .= '};';

		$constructor .= '	
		public '.ucfirst(strtolower($className)).'DAO(Context context) {
			super(context);
		}
		
				function _return'. $className.'FromRow(&$row){
			$'. strtolower($className).' = &new '. ucfirst(strtolower($className)).'();	
			
			$'. strtolower($className).'->set'. ucfirst(strtolower($className)).'Id($row[\''. strtolower($className).'_id\']);';
			
			 foreach($variables as $varable){
				$parts = explode(':',$varable);
				$varable = $parts[0];
				$constructor .= '
			$'. strtolower($className).'->set'.ucfirst(strtolower($varable)) .'($row[\''. strtolower($varable).'\']);';
			}
			

		$constructor .= '
		
			HookRegistry::call(\''. ucfirst(strtolower($className)).'::_return'. ucfirst(strtolower($className)).'FromRow\', array(&$'. strtolower($className).', &$row));
			return $'. strtolower($className).';
		}
	
		function &get'. ucfirst(strtolower($className)).'ById($'. strtolower($className).'Id){
			// First check the in-memory response cache
			if (isset($this->'. strtolower($className).'Cache[$'. strtolower($className).'Id])) {
				return $this->'. strtolower($className).'Cache[$'. strtolower($className).'Id];
			}
			
			$result = &$this->retrieve(
				\'SELECT * FROM '. strtolower($className).' WHERE '. strtolower($className).'_id = ? \', array($'. strtolower($className).'Id)
			);

			$returner = null;
			if ($result->RecordCount() != 0) {
				$returner = &$this->_return'. ucfirst(strtolower($className)).'FromRow($result->GetRowAssoc(false));
			}
			$result->Close();
			unset($result);

			// Cache this '. ucfirst(strtolower($className)).'.
			$this->'. strtolower($className).'Cache[$'. strtolower($className).'Id] =& $returner;

			return $returner;
	
		}
	

	
		function &get'. $className.'s(){
			$result = &$this->retrieveRange(
				\'SELECT * FROM '. strtolower($className).'\'
			);
		
			$returner =& new DAOResultFactory($result, $this, \'_return'. ucfirst(strtolower($className)).'FromRow\');
			return $returner;
		}';
		return $constructor;
		
	 }
	 
	 function getDAONew($className, $variables) {
		
		$new = '
		
		/**
		* Insert a new '. $className.'.
		* @param $'. strtolower($className).' '. ucfirst(strtolower($className)).'
		*/	
	 	function insert'. ucfirst(strtolower($className)).'(&$'. strtolower($className).') {
			$this->update(
				\'INSERT INTO '. strtolower($className).'
					(' ;
					
					foreach ($variables as $varable) {
						$parts = explode(':',$varable);
						$varable = $parts[0];
						$new .= ' 
						'. strtolower($varable).',';
					}
					$new = substr($new, 0, -1);		
					$new .= ')
					VALUES
					(' ;
					
					foreach ($variables as $varable) {
						$parts = explode(':',$varable);
						$varable = $parts[0];
						$new .= '?,';
					}
					$new = substr($new, 0, -1);	
					$new .= ')\',
				array(
					 ';
					
					foreach ($variables as $varable) {
						$parts = explode(':',$varable);
						$varable = $parts[0];
						$new .= '
					 $'. strtolower($className).'->get'.ucfirst(strtolower($varable)).'(),';
					} 
				$new = substr($new, 0, -1);		
				$new .= ')
			);

			$'. strtolower($className).'Id = $this->getInsert'. ucfirst(strtolower($className)).'Id();
			$'. strtolower($className).'->set'. ucfirst(strtolower($className)).'Id($'. strtolower($className).'Id);

			// Cache this response.
			$this->'. strtolower($className).'Cache[$'. strtolower($className).'->get'. ucfirst(strtolower($className)).'Id()] =& $returner;

			return $'. strtolower($className).'->get'. ucfirst(strtolower($className)).'Id();
		}';
		
		return $new;
	}
	
	 function getDAOUpdate($className, $variables) {
		
		$new = '
		
	 	function update'. ucfirst(strtolower($className)).'(&$'. strtolower($className).') {
			return $this->update(
				\'UPDATE '. strtolower($className).' SET ';
					
					foreach ($variables as $varable) {
						$parts = explode(':',$varable);
						$varable = $parts[0];
						$new .= '
						'. strtolower($varable) .' = ?,';
					}
					$new = substr($new, 0, -1);
					$new .= ' 
					WHERE '.strtolower($className).'_id = ?';

					
					$new .= '\',
				array(';
					
					foreach ($variables as $varable) {
						$parts = explode(':',$varable);
						$varable = $parts[0];
						$new .= '
					 $'. strtolower($className).'->get'.ucfirst(strtolower($varable)).'(),';
					} 
					$new .= '
					 $'. strtolower($className).'->get'. ucfirst(strtolower($className)).'Id())';
					
					$new .= '
				);
		}';
		return $new;
	 }
		
	 function getDAOFooter($className) {
		$footer = '
		
		/**
	 	* Get the ID of the last inserted '.strtolower($className).'
		 * @return int
		 */
		function getInsert'. ucfirst(strtolower($className)).'Id() {
			return $this->getInsertId(\''. strtolower($className).'\', \''. strtolower($className).'_id\');
		}
}

?>';
		return $footer;
	 }
	 
	 
	 
	function getHeader($className) {
		$header = '<?php

class '. ucfirst(strtolower($className)).' extends DataObject {
				';
		return $header;
	}
	 

	function getConstructor($className, $variables) {
	
		$con = '
	
	function set'. ucfirst(strtolower($className)).'Id($'. strtolower($className).'Id){
		$this->setData(\''. strtolower($className).'Id\',$'. strtolower($className).'Id);
	}
	function get'. ucfirst(strtolower($className)).'Id(){
		return $this->getData(\''. strtolower($className).'Id\');
	}'. "\r\n";
	
	foreach ($variables as $varable) {
		$parts = explode(':',$varable);
		$varable = $parts[0];
		$con .= '		
	function set'. ucfirst(strtolower($varable)).'($'.strtolower($varable).'){
		$this->setData(\''. strtolower($varable).'\',$'. strtolower($varable).');
	}				
	function get'. ucfirst(strtolower($varable)).'(){
		return $this->getData(\''. strtolower($varable).'\');
		
	}'. "\r\n";
	}

	return $con;
	
	}
	
	 function getFooter($className) {
		$footer = '
}

?>';
		return $footer;
	 }



	function getSQLCreate($className, $variables) {
		
		$sql = '
		CREATE TABLE '. strtolower($className).' (
			'. strtolower($className).'_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,';
		  foreach ($variables as $varable) {
			$parts = explode(':',$varable);
			$varable = $parts[0];
			$varableType = $parts[1];
			$sql .= '
			'. strtolower($varable).' '. strtoupper($varableType).' ,';
		  }
		$sql .= '
			PRIMARY KEY ('. strtolower($className).'_id)
		)
		ENGINE = MyISAM;';
		return $sql;

	}
	

	/** Get Vars from Form **/
	$className= $_POST['className'];
	$classVariables = $_POST['classVariables'];	
	$variables = explode("\r\n",$classVariables);
 	 
	/** Build Paths **/
	$paths = array();
	$paths['root'] = './builds';
	$paths['object_name'] = './builds/'.strtolower($className);
	$paths['classes'] = './builds/'.strtolower($className).'/classes';
	$paths['class_object'] = './builds/'.strtolower($className).'/classes/'.strtolower($className);
	$paths['db'] = './builds/'.strtolower($className).'/classes/'.strtolower($className).'/db';
	
	/** Build Files **/
	$files = array();
	$files['dao'] = ucfirst(strtolower($className)).'DAO.inc.php';
	$files['object'] = ucfirst(strtolower($className)).'.inc.php';
	$files['sql'] = ucfirst(strtolower($className)).'.sql';

	
	/** Make Dirs **/
	@mkdir($paths['root']);
	@mkdir($paths['object_name']);
	@mkdir($paths['classes']);
	@mkdir($paths['class_object']);
	@mkdir($paths['db']);
	
	/** Make Files **/
	$daoFile = fopen( $paths['class_object'].'/'. $files['dao'] ,'w+');	
	$dataObjectFile = fopen( $paths['class_object'].'/'. $files['object'] ,'w+');
	$dbFile = fopen($paths['db'] .'/'. $files['sql'] ,'w+');


	 fwrite ($daoFile,getDAOHeader($className));
	 fwrite ($daoFile,getDAOConstructor($className, $variables));
	 fwrite ($daoFile,getDAONew($className, $variables));
	 fwrite ($daoFile,getDAOUpdate($className, $variables));
	 fwrite ($daoFile,getDAOFooter($className));
	    
	 fwrite ($dataObjectFile,getHeader($className));
	 fwrite ($dataObjectFile,getConstructor($className, $variables));
	 fwrite ($dataObjectFile,getFooter($className));
	

	 
	 fwrite ($dbFile,getSQLCreate($className, $variables));
	 
	 
	 fclose ($daoFile);
	 fclose($dataObjectFile);
	 fclose($dbFile);
	 
	 echo "<a href='index.php'>Build Another Model</a>";
}

?>