<h1>Object Creation Form</h1>
<p>Enter the object name and list the attributes in the text box with the following format:</p>
<form action="generate.php" method="post">
<table border="0">
	<tr>
		<td>
 			<p><b>Class Name:</b></p>
 		</td>
 		<td>
 			<input type="text" name="className" />
 		</td>
 	</tr>
 	<tr>
 		<td valign="top">
 			<p><b>Class Attributes:</b></p>
 		</td>
 		<td>
			<textarea name="classVariables" cols="45" rows="25">attribute1:varchar(45)</textarea>
 		</td>
 	</tr>
 	<tr>
 		<td colspan="2" align="right">
 			<input name="submit" type="submit" value="Submit" />
 		</td>
 	</tr>
 </table>
</form>
